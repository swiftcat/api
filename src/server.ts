import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as express from 'express';
import * as logger from 'morgan';
import * as path from 'path';
import * as errorHandler from 'errorhandler';
import * as cors from 'cors';
import * as mongoose from 'mongoose';
import { router } from './routes';
 
export class Server {

    public app: express.Application;
    public mongoose: any;
    private readonly EXECUTE_PORT: number | string | any = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8181;
    private readonly EXECUTE_HOST: string =  process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';
    private readonly DB_CONNECTION_PATH: string = 'mongodb://swiftcat:swiftcat1@ds111012.mlab.com:11012/swiftcat-tst';

    constructor() {
        this.app = express();
        this.mongoose = mongoose;
        this.config();
    }

    public static bootstrap(): Server {
        return new Server();
    }

    public config(): void {
        this.app.use(express.static(path.join(__dirname, "public")));
        this.app.use(logger("dev"));
        this.app.use(cors());
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));
        this.app.use(cookieParser("SECRET_GOES_HERE"));
        this.app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
            err.status = 404;
            next(err);
        });
        this.app.use(errorHandler());
        this.app.use(router);
        this.mongoose.connect(this.DB_CONNECTION_PATH, { useNewUrlParser: true }).then(() => {
            this.startListeninServer();
        });

        
    }

    private startListeninServer(): void {
        this.app.listen(this.EXECUTE_PORT, this.EXECUTE_HOST, () => {
            console.log('api is work');
        });
    }
}
