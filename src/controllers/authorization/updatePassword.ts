import { Request, Response } from 'express';
import { switchMap, map } from 'rxjs/operators';
import { of } from 'rxjs';

import * as crypto from 'crypto';

import { Methods } from './../../shared/db.methods';
import { errorMessages } from '../../shared/config/error-messages';
import { userSchema } from '../../shared/schemas/user.schema';
import { User } from '../../shared/models/user.model';

export default class UpdatePassword extends Methods {

    public static handleUpdatePasswordRoute(request: Request, response: Response): void {
        if (!request.body.hasOwnProperty('user_id') || !request.body.hasOwnProperty('password') 
        || !request.body.hasOwnProperty('refresh_token') || !request.body.hasOwnProperty('expire')) 
        {
            response.status(404).send(errorMessages.objectError);
            return;
        }

        UpdatePassword.findeUser$(request).then((user: User) => {
            if (!user) { 
                response.status(404).send(errorMessages.objectError); 
                return; 
            } 

            if (user.user_token.refresh_token.expire > Number(new Date())) {
                response.status(403).send(errorMessages.tokenTimeout); 
                return;
            }

            
            UpdatePassword.updatePassword$(request).then((isUpdate: boolean) => {

                if (!isUpdate) {
                    response.status(500).send(errorMessages.passwordUnSave); 
                }

                response.status(200).send(true);
            });
        });
    }

    public static async updatePassword$(request: Request): Promise<boolean> {
        request.body.password = crypto.createHash('md5').update(request.body.password).digest('hex');
        return Methods.update<boolean>(userSchema.returnUserSchema() ,{_id: request.body.user_id }, { $set: { password: request.body.password }}).toPromise();
    }

    public static async findeUser$(request: Request): Promise<User> {
        return Methods.findOne<User>(userSchema.returnUserSchema(), { _id: request.body.user_id }).pipe(
            switchMap((user: User) => {
                if (!user) { return of(null) }
                return of(user);
            })
        ).toPromise();
    }
}