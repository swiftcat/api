import * as crypto from 'crypto';

import { Observable } from 'rxjs';
import { Request, Response } from 'express';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

import { userSchema } from '../../shared/schemas/user.schema';
import { User } from '../../shared/models/user.model';;
import { Token } from '../../shared/models/token.model';
import { Methods } from '../../shared/db.methods';
import { errorMessages } from '../../shared/config/error-messages';

export default class Authorization extends Methods {

    constructor() {
        super();
    }

    public static handleAuthRoute(request: Request, response: Response):void {

        if (!request.body.hasOwnProperty('username') || !request.body.hasOwnProperty('password')) {
            response.status(404).send(errorMessages.objectError);
            return;
        }

        request.body.password = crypto.createHash('md5').update(request.body.password).digest('hex');

        Authorization.fineUser$(request.body).then(
            (user: User) => {
                if (!user) { 
                    response.status(401).send(errorMessages.loginFaild); 
                    return; 
                }

                const token: Token = new Token(user);

                Authorization.updateToken(user, token)
                    .subscribe(() => response.status(200).send(token.access_token));
            }, 
            (error: Error) => {
                response.status(500).send(errorMessages.internalError + error)
            }
        );
    }

    private static updateToken(data: User, token: Token): Observable<boolean> {
        return Methods.update<boolean>(userSchema.returnUserSchema() ,{_id: data._id }, { $set: { user_token: token }});
    }

    private static async fineUser$(user: User): Promise<User> {
        return Methods.findOne<User>(userSchema.returnUserSchema(), user, {}).pipe(
            switchMap((user: User) => {
                if (!user) { return of(null) };
                return of(user);
            })
        ).toPromise();
    }
}