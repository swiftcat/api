import * as nodemailer from 'nodemailer';
import { Request, Response } from 'express';

import { Methods } from './../../shared/db.methods';
import { User } from './../../shared/models/user.model';
import { userSchema } from '../../shared/schemas/user.schema';
import { confirmTemplate } from '../../shared/templates/confirm-recovery';
import { Token } from '../../shared/models/token.model';
import { errorMessages } from '../../shared/config/error-messages';

export default class Recovery extends Methods {

    public static handleRecoveryRoute(request: Request, response: Response): void {

        if (!request.body.hasOwnProperty('email')) {
            response.status(404).send(errorMessages.objectError);
            return;
        }

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'tthwacka@gmail.com',
                pass: '01031996412369'
            }
        });

        const newToken = new Token();

        const modifyConfig = {
            query: request.body,
            update: { user_token: newToken },
            upsert: true
        };
        Methods.findOneAndUpdate<User>(userSchema.returnUserSchema(), modifyConfig).subscribe(
            (updatedUser: User) => {
                if (!updatedUser) {
                    response.status(400).send(errorMessages.emailNotFound);
                    return;
                }
                const mailOptions = {
                    from: 'tthwacka@gmail.com',
                    to: updatedUser.email,
                    subject: 'Swiftcat - Recovery your password',
                    html: confirmTemplate(newToken.refresh_token.key, updatedUser._id, newToken.refresh_token.expire),
                };

                transporter.sendMail(mailOptions, (error, info) => {
                    if (info) {
                        response.status(200).send({emailSend: true, email: updatedUser.email});
                    }
                }); 
        }, (error: Error) => {
            response.status(500).send(errorMessages.internalError + error);
        });
    }
}
