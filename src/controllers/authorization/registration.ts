import * as crypto from 'crypto';
import { Request, Response } from 'express';
import { Methods } from "../../shared/db.methods";
import { userSchema } from '../../shared/schemas/user.schema';
import { errorMessages } from '../../shared/config/error-messages';

interface IUpdateResponse {
    n: number,
    nModified: number,
    ok: number,
    upserted: any;
}


export default class Registration extends Methods {

    constructor() {
        super();
    }

    public static handleRegistrationRoute(request: Request, response: Response): void {

        if (!request.body.hasOwnProperty('password') || !request.body.hasOwnProperty('username') || !request.body.hasOwnProperty('email')) { 
            return; 
        }

        request.body.password = crypto.createHash('md5').update(request.body.password).digest('hex');
        Methods.updateOne<IUpdateResponse>(userSchema.returnUserSchema(), {
            $or: [
                { username: request.body.username },
                { email: request.body.email }
            ]
        }, { $setOnInsert: request.body }, { upsert: true }).subscribe(
            (signUpResponse: IUpdateResponse) => {
            if (!signUpResponse.upserted) {
                response.status(400).send(errorMessages.registerDataTaken);
                return;
            }
            response.status(200).send(true);
        }, (error: Error) => {
            response.status(500).send(errorMessages.internalError + error);
        });
    }
}