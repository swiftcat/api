export { default as autorization } from './authorization/autorization';
export { default as recovery } from './authorization/recovery';
export { default as registration } from './authorization/registration';
export { default as updatePassword } from './authorization/updatePassword';
