import { Schema, Document, Model } from 'mongoose';
import * as mongoose from 'mongoose';

interface IUserSchemaTypes {
    type: any | null;
    unique: boolean;
    required: boolean;
}

export interface IUserSchema extends IUserSchemaTypes , mongoose.Document {}

export class UserSchema {

    public userSchema: Model<IUserSchema>;

    constructor() {
        this.configUserSchema();
    }

    private configUserSchema(): void {

        const requiredType: IUserSchemaTypes = { type: String, unique: true, required: true };
        const unRequiredType: IUserSchemaTypes = { type: String, unique: false, required: false, };
        const data = {
            username: requiredType, 
            password: requiredType,
            name: unRequiredType,
            secondName: unRequiredType,
            email: unRequiredType,
            user_token: { 
                access_token: { key: String, expire: Number },
                refresh_token: { key: String }
            }
        }
        const schema = new Schema(data, { collection: 'user' });
        this.userSchema = mongoose.model<IUserSchema>('users', schema, 'users');
    }

    public returnUserSchema(): Model<IUserSchema> {
        return this.userSchema;
    }
}

export const userSchema = new UserSchema();