import { Observable } from 'rxjs';
import { FindOneOptions } from 'mongodb';
import { User } from './models/user.model';

interface IUpdateResponse {
    n: number,
    nModified: number,
    ok: number,
}

type IParams = User | { _id: string } | any;

export class Methods {

    constructor() {}

    public static findOne<T>(schema: any, args: IParams, returnArgs?: any): Observable<T> {
        return Observable.create((observer) => {
            schema.findOne(args, returnArgs).exec((err: Error, response: FindOneOptions) => {
                observer.next(response);
                observer.complete();
            })
        });
    }

    public static update<T>(schema: any, args: IParams, updateArgs: any): Observable<T> {
        return Observable.create((observer) => {
            schema.update(args, updateArgs).exec((err: Error, response: IUpdateResponse) => {
                if (response.nModified !== 1) {
                    observer.complete();
                    return;
                }
                observer.next(true);
                observer.complete();
            })
        });
    }

    public static updateOne<T>(schema: any, args: IParams, updateArgs: any, setArgs?: any, upsetArgs?: any): Observable<T> {
        return Observable.create((observer) => {
            schema.update(args, updateArgs, setArgs, upsetArgs).exec((err: Error, response: IUpdateResponse) => {
                observer.next(response);
                observer.complete();
            })
        });
    }

    public static findOneAndUpdate<T>(schema: any, args: IParams): Observable<T> {
        return Observable.create((observer) => {
            schema.findOneAndUpdate(args.query, args.update).exec((err: Error, response: any) => {
                observer.next(response);
                observer.complete();
            })
        })
    }
}   