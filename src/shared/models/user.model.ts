import { Token } from "./token.model";

export class User extends Token {
    _id?: string;
    username: string;
    password: string;
    name: string;
    secondName: string;
    email: string;
    user_token?: Token;
}