import * as crypto from 'crypto';
import { User } from './user.model';

export class Token {
    access_token: {
        key: string;
        expire: number;
    };
    refresh_token: {
        key: string;
        expire: number;
    }

    constructor(src?: User) {
        const date = new Date().toISOString();
        this.access_token =  {
            key: crypto.createHash('md5').update(src? date + src.password : date).digest('hex'),
            expire: Date.parse(date) + Math.round(3000000 - 0.5 + Math.random() * (6000000 - 3000000 + 1))
        }
        this.refresh_token = {
            key: crypto.createHash('md5').update(new Date().toISOString()).digest('hex'),
            expire: Number(new Date()) + 172800000,
        }
    }
}