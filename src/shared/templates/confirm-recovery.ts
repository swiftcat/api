export function confirmTemplate(refreshToken: string, userId: string, expire: number) {
    return `
    <div class="container" style="                
        position: fixed;
        left: 0;
        top: 0;
        background-color: #f3f3f3;
        height: 100%;
        width: 100%;"
        margin: auto;
        text-align: center;
    >
        <div class="mail-container" style="
            height: 100%;
            width: 100%;
            font-family: 'Gudea', sans-serif;
            width: 550px;
            height: 275px;
            text-align: center;
            margin: auto;
            padding: 60px 0;
        ">
            <div class="title" style="
                color: #383838;
                font-family: 'Open Sans', sans-serif;
                font-size: calc(10px + 1vw);
                text-align: center;
            ">Recovery password on Swiftcat!</div>
            <div class="secont-title" style="                
                color: #383838;
                font-family: 'Open Sans', sans-serif;
                font-size: 20px;
                margin-top: 55px;
                text-align: center;
                height: 150px;
            ">Your Swiftcat password can be reset by pressing the button below. If you have not requested a new password, please ignore this email.</div>
            <a style="                
            font-size: 14px;
            cursor: pointer;
            width: 170px;
            height: 35px;
            background-color: #0384e2;
            padding: 10px 38px;
            border-radius: 20px;
            border: none;
            outline: none;
            color: white;
            font-size: 15.5px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
            " href="http://localhost:4200/auth/password?refresh_token=${refreshToken}&user_id=${userId}&expire=${expire}">Reset password</a>
        </div>
    </div>`;
}