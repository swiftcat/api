export const errorMessages = {
    loginFaild: 'Autorization failed. Please check your input and try again',
    internalError: 'Internal error.',
    objectError: 'Request object error',
    emailNotFound: 'Email not found! Please try agane or register!', 
    registerDataTaken: 'Username or email is already taken. Please, try again!',
    tokenTimeout: 'Refresh token time out. Please repeat the password recovery procedure.',
    passwordUnSave: 'Internal error. Password dont saved. Please, try agane later with another password! '
};