import * as express from 'express';

import * as auth from './controllers';

const router = express.Router();

router.post('/authentication/auth', auth.autorization.handleAuthRoute);
router.post('/authentication/registration', auth.registration.handleRegistrationRoute);
router.post('/authentication/recovery', auth.recovery.handleRecoveryRoute);
router.post('/authentication/set_password', auth.updatePassword.handleUpdatePasswordRoute);

export { router };